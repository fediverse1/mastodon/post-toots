#!/usr/bin/env python

import datetime
import cv2
import argparse
from mastodon import Mastodon

class post:
    def __init__ (self, vUser, vPassword, vFile):
        f_read = open(vFile, "r")
        instance = f_read.readlines()[-1].strip(" \n")
        f_read.close()
        
        self.mastodon = Mastodon(
            client_id = vFile,
            api_base_url = instance
        )

        self.mastodon.access_token = self.mastodon.log_in(
            username = vUser,
            password = vPassword,
            scopes = ['read', 'write'],
            to_file = '.token'
        )
        
        self.post_date = None
    
    def send (self, vToot, vMedia, vVis):
        self.mastodon.status_post(vToot, media_ids = vMedia, scheduled_at = self.post_date, visibility = vVis)
        
    def add_Media (self, vMedia):
        return (self.mastodon.media_post (vMedia))
    
    def set_date (self, vDate):
        try:
            self.post_date = datetime.datetime.strptime (vDate, '%b %d %Y %H:%M %Z')
        except:
            self.post_date = None
    
    def delete_toot (self, vId):
        self.mastodon.status_delete (vId)

    def delete_s_toot (self, vId):
        self.mastodon.scheduled_status_delete(vId)

    def show_scheduled (self):
        print (self.mastodon.scheduled_statuses ())

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Post toots to you Mastodon account.')
    parser.add_argument ('-a', '--action',      help = '"post", "schedule", "delete", "deletescheduled" or "list"',   default = 'list')
    parser.add_argument ('-u', '--username',    help = 'user\'s email')
    parser.add_argument ('-p', '--password',    help = 'user\'s password')
    parser.add_argument ('-t', '--toot',        help = 'toot text')
    parser.add_argument ('-m', '--media',       help = 'media ("cam" or image, video, etc.)', default = None)
    parser.add_argument ('-w', '--when',        help = 'when to publish toot. Format: "mmm dd YYYY HH:MM Z"', default = None)
    parser.add_argument ('-v', '--visibility',  help = '"public", "unlisted", "private" or "direct"', default = 'public')
    parser.add_argument ('-f', '--file',        help = 'file containing credentials (if it is not .secrets)', default = '.secrets')
    parser.add_argument ('-i', '--id',          help = 'a toot\'s id (you\'ll only need this for deleting a toot)')

    args=parser.parse_args()
    
    p=post(args.username, args.password, args.file)
    
    mmedia = args.media
    if mmedia != None:
        if mmedia == "cam":
            camera=cv2.VideoCapture(0)
            return_value, image =camera.read() 
            cv2.imwrite("pic.png", image)
            del(camera)
            mmedia="pic.png"

        mmedia = p.add_Media(mmedia)
    
    if args.action == 'post':
        p.send (args.toot, mmedia, args.visibility)

    elif args.action == 'schedule':
        p.set_date (args.when)
        p.send (args.toot, mmedia, args.visibility)
    
    elif args.action == 'delete':
        p.delete_toot (args.id)

    elif args.action == 'deletescheduled':
        p.delete_s_toot (args.id)

    else:
        p.show_scheduled()
