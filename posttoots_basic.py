#!/usr/bin/env python

import sys
from mastodon import Mastodon

mastodon = Mastodon (
    client_id = '.secrets',
)

mastodon.access_token = mastodon.log_in (
    username = sys.argv[1],
    password = sys.argv[2],
    scopes = ['read', 'write']
)

mastodon.status_post (sys.argv[3])
