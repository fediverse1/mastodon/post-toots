# Post Toots

Post and schedule toots to your Mastodon account from the command line.

## Download

Download a zip, tar, gzip or bzip from the links above, or clone the repository with

```
git clone https://gitlab.com/fediverse1/mastodon/post-toots
```

## Installing dependencies

*posttoots.py* needs the [Mastodon.py](https://pypi.org/project/Mastodon.py/) and OpenCV modules to work. You can install them with

```
pip install Mastodon.py opencv-python
```

### Using the Python environment

Or, if you prefer not to sully your system, you can use the integrated Python environment. ```cd``` into the *post-toots/* directory and run:

```
source mastodon-test/bin/activate
```
When you are done sending yout toots out, you can deactivate the environment with:

```
deactivate
```

## Registering your app

Before you can use *posttoots.py* you must register the app with your Mastodon instance. To do this, use the *posttoots_register.py* utility:

Make the app executable with

```
chmod a+x posttoots_register.py
```

Then you can run it with

```
./posttoots_register.py -i https://your.mastodon.instance -s read -s write
```

(*posttoots.py* is going to need both read and write access to your account).

You only need to run *posttoots_register.py* once. **DO NOT** run the registration utility every time you run *posttoots.py*. Unless told otherwise, *posttoots_register.py* will save some credentials in a file called *.secrets* in the app's directory.


Use 

```
./posttoots_register.py -h
```

to get help.

## Running *postoots.py*

Now make *posttoots.py* executable with

```
chmod a+x posttoots.py
```

And use the ```-h``` flag to get help:

```
./posttoots-py -h
```

This will show:

```
usage: posttoots.py [-h] [-a ACTION] [-u USERNAME] [-p PASSWORD] [-t TOOT]
                    [-m MEDIA] [-w WHEN] [-v VISIBILITY] [-f FILE]

Post toots to you Mastodon account.

optional arguments:
  -h, --help            show this help message and exit
  -a ACTION, --action ACTION
                        "post", "schedule" or "list"
  -u USERNAME, --username USERNAME
                        user's email
  -p PASSWORD, --password PASSWORD
                        user's password
  -t TOOT, --toot TOOT  toot text
  -m MEDIA, --media MEDIA
                        media ("cam" or image, video, etc.)
  -w WHEN, --when WHEN  when to publish toot. Format: "mmm dd YYYY HH:MM Z"
  -v VISIBILITY, --visibility VISIBILITY
                        "public", "unlisted", "private" or "direct"
  -f FILE, --file FILE  file containing credentials (if it is not .secrets)
```

### Examples

Post a simple text toot:

```
./posttoots.py -u your@email.org -p "Your password" -t "I post from the command line!" -a post
```

Post a toot with a text and an image:

```
./posttoots.py -u your@email.org -p "Your password" -t "This picture is pretty" -m /path/to/picture.png -a post
```

Send a private message to your own inbox:

```
./posttoots.py -u your@email.org -p "Your password" -t "This is a private message" -a post -v direct
```

Send a private message to your own inbox and a friend's:

```
./posttoots.py -u your@email.org -p "Your password" -t "This is a private message for @myfriend@some.mastodon.instance" -a post -v direct
```

Delete a toot or private message:

```
./posttoots.py -u your@email.org -p "Your password" -a delete -i 103206102501997960  # <<< Toot's id number
```

Schedule a toot with a text and an image:

```
./posttoots.py -u your@email.org -p "Your password" -t "This picture is pretty" -m /path/to/picture.png -a schedule -w "dec 06 2019 20:25 CET"
```

Delete a scheduled toot:

```
./posttoots.py -u your@email.org -p "Your password" -a delete -i 84 # <<< Scheduled toot's id number
```

See a list of all your scheduled toots:

```
./posttoots.py -u your@email.org -p "Your password"
```

## How to Send a Toot with Line Breaks

Use the ```-t``` option AT THE END of the line and type something like this:

```
./posttoots.py -u your@email.org -p "Your password" -a post -v direct -t "A text
```

And press [Enter].

As you haven't closed the quotes ("), Bash will take you to a new line and you can continue writing your post. Continue writing your message on the next line and press [Enter] without closing your quotes.

Continue until you have finished your message **AND THEN CLOSE THE QUOTES**:

```
./posttoots.py -u your@email.org -p "Your password" -a post -v direct -t "A text
> with 
> linebreaks"
```

This will deliver a post  containing line breaks to your inbox.

## Caveats

Although Mastodon statuses can take up to four images per toot, at the moment the tool is only able to upload one media file per toot. 
