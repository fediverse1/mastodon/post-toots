#!/usr/bin/env python3

from mastodon import Mastodon
import argparse

parser = argparse.ArgumentParser(description='Register a Mastodon app.')

parser.add_argument ('-f', '--file',    help = 'file where you want to store credentials (default: .secrets)', default = '.secrets')
parser.add_argument ('-i', '--instance',help = 'Mastodon instance', default = 'https://mastodon.social/')
parser.add_argument ('-s', '--scopes',  help = 'Scope ("read", "write", "follow", "push" -- use multiple times for more than one scope)', action='append')

args=parser.parse_args()

Mastodon.create_app(
     'posttoots',
     api_base_url = args.instance,
#     scopes= args.scopes,
     to_file = args.file
)

mastodon = Mastodon (
    client_id = args.file
)

print ("Mastodon version: " + mastodon.retrieve_mastodon_version())
